# Exercice 1 TP1

## Objectif
L'objectif de cet exercice est de créer un script python permettant d'effectuer les 4 opérations élémentaires (addition, soustraction, division, multiplication)

## Réalisation
Pour ce faire j'ai créer un script contenant 4 fonctions effectuant chacune une des quatres opérations. Lorsque qu'on lance le programme, il est demandé à l'utilisateur de saisir deux entiers, les opérations se feront sur ces deux entiers.

## Lancement
Pour lancer le programme, il faut se placer dans le dossier tp1_ex1 puis lancer la commande `python exo_1.py`.
