#!/usr/bin/env python
# coding: utf-8
"""
author : paul Bouyssoux

"""

def fsum(a,b):
    return a + b

def substract(a,b):
    return a - b

def multiply(a,b):
    return a * b

def divide(a,b):
    if (b !=0):
        return a / b
    else:
        return("vous avez essayé de diviser par zéro")



if __name__ == '__main__':
    a = int(input("entrez le premier entier : "))
    b = int(input("entrez le second entier : "))
    print("somme : ", fsum(a,b))
    print("soustraction : ", substract(a,b))
    print("multiplication : ", multiply(a,b))
    print("division : ", divide(a,b))
